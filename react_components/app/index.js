import React from 'react';
import ReactDOM from 'react-dom';


import CommentsList from './components/CommentsList';


export default {
		CommentsList: {
            attachToId: ({attachId, ...p}) => ReactDOM.render(<CommentsList {...p}/>, document.getElementById(attachId))
     },


};
