"use strict";

import React, {useEffect, useState} from 'react';

const request = async(url, method) => {
    const params = {
        'cache': 'no-cache',
        method,
        'headers':  {
            "Accept": "application/json",
        },
    }
    return fetch(url, params).then(response => response.json()).catch(err => {
        throw new Error('Higher-level error. ' + err.message);
    })
}

const CommentsList = (props) => {

    const [state, setState] = useState({ 
        isLoaded: false, 
        comments: [],
    })

    useEffect(() => {
        loadComments()
    }, [])

    const loadComments = async() => {

        try {
            const comments = await request("api.php?comments", "get");
            setState(prev => ({
                ...prev,
                isLoaded: true, 
                comments
                }
            ));
        } catch (err) {
            alert(err)
        }
    }

    if(state.isLoaded) {
        return (
        <table border="1">
        <tbody>
        {state.comments.map((v, i) => (
        <tr key={i}><td>{v.id}</td><td>{v.text}</td><td>{v.created_at}</td></tr>
        ))}
        </tbody>
        </table>

       )
	   } else {
	    return null;
	   }
}
export default CommentsList;
